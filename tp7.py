import cv2
import numpy as np

#Coordenadas para la trasnformacion afin
points1 = [[-1, -1], [-1, -1], [-1, -1]] 
points2 = [[-1, -1], [-1, -1], [-1, -1]]
i = 0

def selec_points(event, x, y, flags, param): #selecciona puntos en la imagen
    global points2, i
    
    if event == cv2.EVENT_LBUTTONUP:
        if i == 0 :
            points2[0] = x, y
            cv2.circle(img1_aux, (x, y), 5, (0, 255, 0) , -1) #crea los circulos verdes sobre el lugar seleccionado
            i = i + 1
        elif i == 1 :
            points2[1] = x, y
            cv2.circle(img1_aux, (x, y), 5, (0, 255, 0) , -1)
            i = i + 1
        elif i == 2 :
            points2[2] = x, y
            cv2.circle(img1_aux, (x, y), 5, (0, 255, 0) , -1)
            i = 0

def Trans_affin(image): #genera la imagen nueva con la incrustación
    h, w = image.shape[:2] #obtengo las dimensiones de imagen2
    pts1 = np.float32([[0,0], [w, 0], [w,h]]) #fijo los puntos previos a la transformacion
    pts2 = np.float32([[points2[0]], [points2[1]], [points2[2]]]) #configuro los puntos a los que se transforman los anteriores
    M = cv2.getAffineTransform(pts1, pts2) #obtengo la matriz de la transformacion
    final = cv2.warpAffine(image, M, (w,h)) #realizo la transformacion de img2
    return final

img1 = cv2.imread('arbol.png', 1) #leo la imagen del fondo
img2 = cv2.imread('hoja_sola.png', 1) #leo la imagen que se incrusta

(h, w) = img1.shape[:2] #obtengo las dimensiones de imagen1
img2 = cv2.resize(img2, (w, h)) #redimensiono 2 para poder hacer la mascara despues de la transformada

img1_aux = img1.copy() #imagen auxiliar para que los puntos dibujados no formen parte de la imagen final
cv2.namedWindow('Arbol')
cv2.imshow('Arbol', img1_aux)
cv2.setMouseCallback('Arbol', selec_points) #ejecuta la funcion selec_points cuando hay eventos del mouse sobre la ventana

while(1):
    cv2.imshow('Arbol', img1_aux)
    k = cv2.waitKey(1) & 0xFF
    if k == ord('a'):       
        img_transf = Trans_affin(img2)
        
        #hago una mascara para poner la img2 sobre img1
        mask = img_transf.copy() #copio la imagen transformada 
        mask[mask != 0] = 255 #hace que todos los puntos distintos de 0 en la mascara se pongan en blanco
        #entonces la mascara son todos los puntos en blanco
        mask = cv2.bitwise_not(mask) #invierte los bits, ahora la mascara son los pixeles negros
        mask = cv2.bitwise_and(mask, img1) #inserta la mascara en img1
        #la mascara recorta la hoja si el fondo es negro, si es de otro color, el fondo queda en la imagen final
        final = cv2.bitwise_or(mask, img_transf) #vuelve a poner los pixeles de la mascara en los colores correctos
        cv2.imwrite('Resultado.jpg', final)
        cv2.namedWindow('Resultado')
        cv2.imshow('Resultado', final)       
    elif k == ord('q'):
        break

cv2.waitKey(0)      
cv2.destroyAllWindows()
