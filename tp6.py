import cv2
import numpy as np

drawing = False # true si se presiona el mouse
ix, iy = -1, -1
fx, fx = -1, -1

def crop_rectangle(event, x, y, flags, param):
    global ix, iy, fx, fy, drawing
    if event == cv2.EVENT_LBUTTONDOWN: #pulsacion del boton izquierdo
        drawing = True
        ix, iy = x, y      #define el inicio del recuadro
    elif event == cv2.EVENT_MOUSEMOVE: #movimiento del mouse
        if drawing is True:
            fx, fy = x, y  #cambia el final del recuadro si drawing==True
    elif event == cv2.EVENT_LBUTTONUP: #dejar de apretar boton izquierdo
        drawing = False     #deja de dibujar el recuadro
        cv2.rectangle(img, (ix, iy), (fx, fy), (0, 255, 0) , 1) #genera el recuadro que se ve sobre la imagen

def comparo_infin():
    global ix, iy, fx, fy
    if iy<=fy and ix<=fx:   #comparo los valores de incio y fin para que el recuadro se pueda hacer en cualquier sentido de desplazamiento
        return(ix,fx,iy,fy)
    elif iy>fy and ix<=fx:
        return(ix,fx,fy,iy)
    elif iy<=fy and ix>fx:
        return(fx,ix,iy,fy)
    else:
        return(fx,ix,fy,iy)

def transf_euclidiana(image, tx, ty, s, angle, h, w):
    angle_rad = angle * np.pi/180 #convierto a radianes para calcular correctamente sen y cos
    M = np.float32([[s*np.cos(angle_rad), s*np.sin(angle_rad), tx], #matriz de transformación euclidiana
                    [-(s*np.sin(angle_rad)), s*np.cos(angle_rad), ty]])
    cosM = np.abs(M[0][0]) #cos y sen del ángulo para calcular el ancho y alto de la imagen final
    senM = np.abs(M[0][1])
    newImageHeight = int((w * senM) + (h * cosM)) + ty #alto final
    newImageWidth = int((w * cosM) + (h * senM)) + tx #ancho final
    if angle<=90: #según el angulo de rotación desplazo la imagen rotada
        M[1][2] += senM * w
    elif angle>90 and angle<180:
        M[0][2] += np.sin(angle_rad - np.pi/2) * w
        M[1][2] += newImageHeight
    elif angle>=180 and angle<270:
        M[0][2] += newImageWidth
        M[1][2] += np.cos(angle_rad - np.pi) * h
    elif angle>=270 and angle<360:
        M[0][2] += np.cos(angle_rad - (3/2)*np.pi) * h

    rotated = cv2.warpAffine(image, M,(newImageWidth, newImageHeight)) #genero la nueva imagen teniendo en cuenta M y el nuevo alto y ancho
    return rotated

img = cv2.imread('hoja.png', 1) #lee la imagen
aux_img = img.copy() #crea una copia de la imagen completa, sin recortar
cv2.namedWindow('image') #crea la ventana donde se muestra la imagen
cv2.setMouseCallback('image', crop_rectangle) #ejecuta la funcion crop cuando hay eventos del mouse sobre la ventana

while(1):
    cv2.imshow('image', img)
    k = cv2.waitKey(1) & 0xFF
    if k == ord('g'): #al presionar g guarda el recorte 
        (ai,af,bi,bf)=comparo_infin()
        crop_img = img[bi:bf, ai:af]
        cv2.imwrite('recorte.png', crop_img)
    elif k == ord('s'): #al presionar e realiza la transformación euclidiana 
        cv2.destroyWindow("image")
        (ai,af,bi,bf)=comparo_infin() 
        crop_img = img[bi:bf, ai:af]

        (h, w) = crop_img.shape[:2] #obtengo altura y ancho del recorte
        angle = int(input('Ingrese el angulo de rotacion expresado en grados: '))
        tx = int(input('Ingrese la traslacion en x: '))
        ty = int(input('Ingrese la traslacion en y: '))
        s = float(input('Ingrese la escala: '))
        new_img = transf_euclidiana(crop_img, tx, ty, s, angle, h, w)
        cv2.imwrite('transformacion.png', new_img)
        cv2.destroyWindow("image")
    elif k == ord('r'): #al presionar r restaura la imagen original
        img = aux_img.copy()
    elif k == ord('q'): #q para salir
        break
cv2.destroyAllWindows()