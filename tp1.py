def adivina (intentos):
    import random
    numero = random.randint(0, 100) #genera el numero entero aleatorio entre 0 y 100
    print("El numero generado es: ", numero)
    i = 1

    while i<=intentos: 
        print("\nRestan "+str(intentos-i+1)+" intentos")
        a=int(input("Ingrese el numero:"))
        if a==numero: #compara para ver si el numero ingresado es igual al aleatorio
            return i #si se adivinó, se devuelve la cantidad de intentos usados
            i = intentos+5 #se sobrepasa la cantidad de intentos permitidos en el contador para dejar de ejecutar el while
        else:
            print("\nNo ha adivinado")
            i += 1 #si no se adivina se incrementa el contador 
    if i!=(intentos+5): 
        return 0 #si el contador es distinto al valor asignado al adivinar, devuelve cero para indicar que se terminaron los intentos sin adivinar

intentos = int(input("Ingrese la cantidad de intentos permitidos: "))
realizados = adivina (intentos)
if realizados==0: #en base a lo que devuelve la funcion adivina se informa el resultado 
    print("\n\nNo lograste adivinar en los intentos permitidos. Perdiste :(")
else:
    print("\n\nFelicitaciones! Adivinaste en "+str(realizados)+" intentos")
