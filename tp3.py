from fileinput import filename
import sys 
import cv2

if(len(sys.argv) > 1): #detecta si se incluyó algun archivo
    filename = sys.argv[1]
else :
    print('Debe pasar el nombre del archivo como primer argumento') 
    sys.exit(0)

cap = cv2.VideoCapture(filename) #abre el video desde el archivo
fourcc = cv2.VideoWriter_fourcc('X', 'V', 'I ', 'D') #establece el codec de video a utilizar
fps = cap.get(cv2.CAP_PROP_FPS) #obtiene los fps del video de sus propiedades
print(fps) #imprimo para ver los fps
delay = int(1000/fps) #genera el delay en ms según los fps del video
ancho = cap.get(cv2.CAP_PROP_FRAME_WIDTH) #obtiene el ancho de la resolución del video
alto = cap.get(cv2.CAP_PROP_FRAME_HEIGHT) #obtiene el alto de la resolución del video
framesize = (int(ancho), int(alto)) #genera la tupla con la resolución para el archivo de salida
out = cv2.VideoWriter('output.avi', fourcc, fps, framesize) #escribe el archivo de salida

while (cap.isOpened()):
    ret, frame = cap.read() 
    if ret is True:
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY) #operación sobre el video que lo pasa a escala de grises
        out.write(gray)
        cv2.imshow('Image gray', gray) #muestro cada frame generado
        if cv2.waitKey(delay) & 0xFF == ord('g'): #espera el tiempo entre los frames a menos que se presione la g
            break 
    else:
        break

cap.release ()
out.release ()
cv2.destroyAllWindows ()