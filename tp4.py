import cv2
import numpy as np

drawing = False # true si se presiona el mouse
ix, iy = -1, -1
fx, fx = -1, -1

def crop_rectangle(event, x, y, flags, param):
    global ix, iy, fx, fy, drawing
    if event == cv2.EVENT_LBUTTONDOWN: #pulsacion del boton izquierdo
        drawing = True
        ix, iy = x, y      #define el inicio del recuadro
    elif event == cv2.EVENT_MOUSEMOVE: #movimiento del mouse
        if drawing is True:
            fx, fy = x, y  #cambia el final del recuadro si drawing==True
    elif event == cv2.EVENT_LBUTTONUP: #dejar de apretar boton izquierdo
        drawing = False     #deja de dibujar el recuadro
        cv2.rectangle(img, (ix, iy), (fx, fy), (0, 255, 0) , 1) #genera el recuadro que se ve sobre la imagen

def comparo_infin():
    global ix, iy, fx, fy
    if iy<=fy and ix<=fx:   #comparo los valores de incio y fin para que el recuadro se pueda hacer en cualquier sentido de desplazamiento
        return(ix,fx,iy,fy)
    elif iy>fy and ix<=fx:
        return(ix,fx,fy,iy)
    elif iy<=fy and ix>fx:
        return(fx,ix,iy,fy)
    else:
        return(fx,ix,fy,iy)

img = cv2.imread('hoja.png', 1) #lee la imagen
aux_img = img.copy() #crea una copia de la imagen completa, sin recortar
cv2.namedWindow('image') #crea la ventana donde se muestra la imagen
cv2.setMouseCallback('image', crop_rectangle) #ejecuta la funcion crop cuando hay eventos del mouse sobre la ventana

while(1):
    cv2.imshow('image', img)
    k = cv2.waitKey(1) & 0xFF
    if k == ord('g'): #al presionar g guarda el recorte 
        (ai,af,bi,bf)=comparo_infin()
        crop_img = img[bi:bf, ai:af]
        cv2.imwrite('recorte.png', crop_img)
    elif k == ord('r'): #al presionar r restaura la imagen original
        img = aux_img.copy()
    elif k == ord('q'): #q para salir
        break
cv2.destroyAllWindows()