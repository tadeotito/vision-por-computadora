import cv2

img = cv2.imread('hoja.png', 0) #lee la imagen
res=img[:] #crea una copia de la matriz de la imagen en escala de grises

for i in range (len(img)): #sentencia que recorre las filas
    for j in range(len(img[0])): #sentencia que recorre las columnas
        #255 es el blanco y cuanto menor es el valor, mas oscuro es el gris
        if img[i][j]<=200: #compara el valor del gris del pixel con un umbral y decide si ponerlo en blanco o negro en la copia
            res[i][j]=0
        else:
            res[i][j]=255 

cv2.imwrite('resultado.png', res) #crea la imagen resultante en base al color de los pixeles en la matriz res
