import cv2
import numpy as np
from math import sqrt

points = np.float32([[196, 186], [668, 255], [676, 860], [279, 1074]]) #puntos de la seccion de pared a transformar
points_puerta = np.float32([[449, 230], [657, 259], [664, 864], [486, 960]]) #puntos de la puerta a transformar
points2 = [[-1, -1], [-1, -1], [-1, -1],[-1,-1]]
points3 = [[-1, -1], [-1, -1]]
i = 0
drawing = False

def trans_puerta(img1): #transforma la puerta y agrega las medidas
    global points_puerta
    w, h = 840, 2000 #ancho y alto de la puerta
    pts2 = np.float32([[0,0], [w, 0], [w, h], [0, h]]) #puntos de la imagen a la que se mapea con la relacion de aspecto real
    M = cv2.getPerspectiveTransform(points_puerta, pts2) #matriz de la transformación
    img_transf = cv2.warpPerspective(img1, M, (w, h)) #imagen transformada
    cv2.line(img_transf, (0,0), (0,h), (0,255,0),2) #linea vertical
    cv2.putText(img_transf, '2.000 m', (10, 1000), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,255,0), 2 , cv2.LINE_AA) #texto con la medida vertical
    cv2.line(img_transf, (0,0), (w,0), (0,255,0),2) #linea horizontal
    cv2.putText(img_transf, '0.84 m', (400, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,255,0), 2 , cv2.LINE_AA) #texto con la medida horizontal
    return img_transf

def trans_perspectiva(img1): #transformacion de la seccion de pared
    global points
    w, h = 1600, 2030 #ancho y alto proporcional al real de la seccion
    pts2 = np.float32([[0,0], [w, 0], [w, h], [0, h]])
    M = cv2.getPerspectiveTransform(points, pts2)
    img_transf = cv2.warpPerspective(img1, M, (w, h))
    return img_transf

def dist(pt1, pt2): #calcula la distancia entre dos puntos
	a = pt2[0] - pt1[0]
	b = pt2[1] - pt1[1]
	return np.sqrt((a ** 2) + (b ** 2))

def select_points(event,x,y,flags,param): #selecciona puntos en la imagen
    global points2, points3, i, drawing

    if event == cv2.EVENT_LBUTTONDOWN: 
        drawing = True
        points3[i] = x,y #guarda en points3[0 y 1] los puntos inicial y final
        i = i + 1

        if i == 2: #cuando ya se marcaron los dos puntos inicio y fin
            img_transformada[:] = img_measure[:]
            drawing = False
            i = 0
    
    elif event == cv2.EVENT_MOUSEMOVE:
        if (drawing is True): #si marque el punto inicial pero no el final
            img_measure[:] = img_transformada[:] #borra la linea anterior
            measure = dist_in_meters(points3[0][0],points3[0][1],x,y) #calcula el largo de la linea
            cv2.line(img_measure, (points3[0][0],points3[0][1]), (x,y), (0,255,0),2) #hace la linea
            cv2.putText(img_measure, measure, (x+10, y+10), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,255,0), 2 , cv2.LINE_AA) #pone el texto con la longitud

def dist_in_meters(px1, py1, px2, py2): #Calcula la distancia en metros entre dos puntos
    px = px2 - px1 #tomo la distancia en pixeles en x e y
    py = py2 - py1

    px_m = px/1000 #convierto a metros
    py_m = py/1000 

    result = sqrt((px_m ** 2) + (py_m ** 2)) #calculo el largo de la linea diagonal
    result = round(result,3) #redondeo con 3 decimales
    result = str(result) #convierto a string 
    return (result + ' m')

img1 = cv2.imread('puertas.png', 1) #leo la imagen

img_puerta = trans_puerta(img1) #obtengo la imagen transformada de la puerta con las medidas 
cv2.imwrite('puerta_sola.jpeg', img_puerta)
cv2.namedWindow('Puerta Sola')
cv2.imshow('Puerta Sola', img_puerta) #la muestro en la nueva ventana

img_transformada = trans_perspectiva(img1) #obtengo la imagen transformada de la seccion de pared 
img_transformada_aux = img_transformada.copy() 
img_measure = img_transformada.copy()
cv2.imwrite('puerta_medición.jpeg', img_measure)
cv2.namedWindow('Puertas transformadas')
cv2.imshow('Puertas transformadas', img_transformada) #muestro la imagen transformada
cv2.setMouseCallback('Puertas transformadas',select_points) #ejecuta la funcion selec_points cuando hay eventos del mouse sobre la ventana

while (1): #while infinito
    cv2.imshow('Puertas transformadas', img_measure) #muestro la imagen sobre la que mido
    k = cv2.waitKey(1) & 0xFF
    if k == ord('r'): #restaura la imagen transformada sin las medidas realizadas
        img_transformada = img_transformada_aux.copy()
        img_measure = img_transformada_aux.copy()
        i = 0
    elif k == ord('q'): 
        break

cv2.waitKey(0)
cv2.destroyAllWindows()